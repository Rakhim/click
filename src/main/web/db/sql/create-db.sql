CREATE TABLE DEPARTMENTS (
  id        INTEGER PRIMARY KEY,
  name      VARCHAR(40)
);

CREATE TABLE DEVELOPERS (
  id            INTEGER PRIMARY KEY,
  first_name    VARCHAR(40),
  middle_name   VARCHAR(40),
  last_name     VARCHAR(40),
  id_department INTEGER,
  CONSTRAINT fk_dev_departments FOREIGN KEY (id_department)
    REFERENCES DEPARTMENTS(id),
  phone_number  VARCHAR(12),
  email         VARCHAR(40)
);

CREATE TABLE TAGS (
  id            INTEGER PRIMARY KEY,
  name          VARCHAR(40),
  proved        VARCHAR(1)
);

CREATE TABLE CUSTOMERS (
  id      INTEGER PRIMARY KEY,
  first_name     VARCHAR(255),
  sur_name      VARCHAR(255),
  patr_name     VARCHAR(255),
  phone_number  VARCHAR(255),
  e_mail        VARCHAR(255)
);

create TABLE TASKS (
  id            INTEGER IDENTITY PRIMARY KEY,
  name          VARCHAR(40),
  id_customer   INTEGER,
  id_department INTEGER,
  status        VARCHAR(40),
  description   VARCHAR(40),
  end_date      TIMESTAMP,
  last_update   TIMESTAMP,
  CONSTRAINT fk_tasks_customer_id FOREIGN KEY (id_customer)
   REFERENCES CUSTOMERS(id),
  CONSTRAINT fk_tasks_department_id FOREIGN KEY (id_department)
   REFERENCES DEPARTMENTS(id)
);

CREATE TABLE TASK_TAGS (
  id_task       INTEGER,
  CONSTRAINT fk_desc_task FOREIGN KEY (id_task)
    REFERENCES TASKS(id),
  id_tag        INTEGER,
  CONSTRAINT fk_desc_tags FOREIGN KEY (id_tag)
    REFERENCES TAGS(id),
);

-- ALTER TABLE user ADD FOREIGN KEY (b_id) REFERENCES bookingnumber(b_id);

CREATE TABLE COMPETENCY(
  id              INTEGER PRIMARY KEY,
  id_developer    INTEGER,
  CONSTRAINT fk_copm_developer FOREIGN KEY (id_developer)
    REFERENCES DEVELOPERS(id),
  id_tag         INTEGER,
  CONSTRAINT fk_copm_tags FOREIGN KEY (id_tag)
    REFERENCES TAGS(id),
  cost            DOUBLE,
  approved        BIT(1)
);

CREATE TABLE STOP_LIST (
  id      INTEGER PRIMARY KEY,
  WORD    VARCHAR(255)
);

CREATE TABLE CONTRACTS(
  id_task         INTEGER,
  id_developer    INTEGER,
  status          VARCHAR(40),
  description     VARCHAR(40),
  feedback        INTEGER,
  CONSTRAINT contracts_pk PRIMARY KEY(id_task, id_developer)
);

CREATE TABLE FAVOURITE_DEVELOPERS (
  id_customer      INTEGER,
  CONSTRAINT fk_customer_fav FOREIGN KEY (id_customer)
    REFERENCES CUSTOMERS(id),
  id_developer     INTEGER,
  CONSTRAINT fk_developer_fav FOREIGN KEY (id_developer)
    REFERENCES DEVELOPERS(id),
);