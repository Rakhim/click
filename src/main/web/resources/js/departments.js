
(function () {
    // get departments

    $.ajax({
        method: "GET",
        url: "/department/list",
        success: function (rawData) {
            rawData.forEach(function (item, i) {
                $('#departments').append('<option>'.concat(item, '</option>'));
            });
        },
        error: function () {
            console.log('error scenario');
        }
    });
})();