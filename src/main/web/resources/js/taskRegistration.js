(function () {
    var tags;
    // task registration

    $('#valid-form').on('click', function (e) {
        e.preventDefault();

        registerTask();
    });

    function registerTask() {
        var formData = $('#contact-form').serialize();
        console.log(formData);
        $.ajax({
            method: "GET",
            url: "/task/add",
            data: formData,
            success: function (rawData) {
                rawData.forEach(function (item, i) {
                    addTag(item);
                });
                console.log('success');
            },
            error: function (e) {
                console.log('error scenario');
            }
        });

        var item = document.getElementById('valid-form');
        item.style.display = 'none';

        var item2 = document.getElementById('valid-form2');
        item2.style.display = 'block';

    };

    $('#valid-form2').on('click', function (e) {
        e.preventDefault();

        saveTask();
    });

    function saveTask() {
        var formData = $('#contact-form').serialize();
        console.log(formData);
        $.ajax({
            method: "GET",
            url: "/task/add",
            data: formData,
            success: function (rawData) {
                tags = rawData;
                console.log(rawData);
                console.log('success');
            },
            error: function () {
                console.log('error scenario');
            }
        });

        var tagData = JSON.stringify(tags);
        console.log(tagData);
        $.ajax({
            method: "GET",
            url: "/saveTagForTask",
            data: tagData,
            success: function (rawData) {
                tags = rawData;
                console.log(rawData);
                console.log('success');
            },
            error: function () {
                console.log('error scenario');
            }
        });

        var item2 = document.getElementById('valid-form2');
        item2.style.display = 'none';

        $('#end').append('<div class="tag">'.concat(tag, '</tag>'));
    }

    function addTag(tag) {
        $('#tags').append('<div class="tag">'.concat('Задача создана!', '</tag>'));
    }
})();