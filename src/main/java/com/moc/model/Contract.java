package com.moc.model;

import javax.persistence.*;

@Entity(name = "CONTRACTS")
@IdClass(ContractPK.class)
public class Contract {
    @Id
    @Column(name = "id_task")
    private Long idTask;

    @Id
    @Column(name = "id_developer")
    private Long idDeveloper;

    @Enumerated(EnumType.STRING)
    @Column
    private ContractStatus status;

    @Column
    private String description;

    @Column
    private Integer feedback;

    public Long getIdTask() {
        return idTask;
    }

    public void setIdTask(Long idTask) {
        this.idTask = idTask;
    }

    public Long getIdDeveloper() {
        return idDeveloper;
    }

    public void setIdDeveloper(Long idDeveloper) {
        this.idDeveloper = idDeveloper;
    }

    public ContractStatus getStatus() {
        return status;
    }

    public void setStatus(ContractStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getFeedback() {
        return feedback;
    }

    public void setFeedback(Integer feedback) {
        this.feedback = feedback;
    }
}
