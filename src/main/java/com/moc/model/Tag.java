package com.moc.model;

import javax.persistence.*;

/*      id            INTEGER PRIMARY KEY,
        name          VARCHAR(40),
        proved        VARCHAR(1)*/

@Entity(name = "TAGS")
public class Tag {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column
    private String name;

    @Column
    private boolean proved;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isProved() {
        return proved;
    }

    public void setProved() {
        this.proved = proved;
    }

    @Override
    public String toString() {
        return String.format("{ \"id\": %d, \"name\": %s, \"proved\": %s}", id, name, proved);
    }
}
