package com.moc.model;

public enum TaskStatus {
    Done, InProcess, New
}
