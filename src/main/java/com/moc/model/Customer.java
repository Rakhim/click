package com.moc.model;

import javax.persistence.*;
import java.util.Set;

/*      id      INTEGER PRIMARY KEY,
        first_name    VARCHAR(255),
        sur_name      VARCHAR(255),
        patr_name     VARCHAR(255),
        phone_number  VARCHAR(255),
        e_mail        VARCHAR(255)*/

@Entity(name = "CUSTOMERS")
public class Customer implements SystemUser {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "sur_name")
    private String surName;

    @Column(name = "patr_name")
    private String patrName;

    @Column(name = "phone_number")
    private String phone;

    @Column(name = "e_mail")
    private String email;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "FAVOURITE_DEVELOPERS", joinColumns = @JoinColumn(name = "id_customer"),
            inverseJoinColumns = @JoinColumn(name = "id_developer"))
    private Set<Developer> favDevelopers;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getPatrName() {
        return patrName;
    }

    public void setPatrName(String patrName) {
        this.patrName = patrName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Set<Developer> getFavDevelopers() {
        return favDevelopers;
    }

    public void setFavDevelopers(Set<Developer> favDevelopers) {
        this.favDevelopers = favDevelopers;
    }
}
