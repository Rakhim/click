package com.moc.model;

import javax.persistence.*;

/*      id            INTEGER PRIMARY KEY,
        first_name    VARCHAR(40),
        middle_name   VARCHAR(40),
        last_name     VARCHAR(40),
        id_department INTEGER,
        CONSTRAINT fk_dev_departments FOREIGN KEY (id_department)
        REFERENCES DEPARTMENTS(id),
        phone_number  VARCHAR(12),
        email         VARCHAR(40)*/

@Entity(name = "DEVELOPERS")
public class Developer implements SystemUser {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String surName;

    @Column(name = "middle_name")
    private String patrName;

    @ManyToOne
    @JoinColumn(name = "id_department")
    private Department department;

    @Column(name = "phone_number")
    private String phone;

    @Column
    private String email;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getPatrName() {
        return patrName;
    }

    public void setPatrName(String patrName) {
        this.patrName = patrName;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
