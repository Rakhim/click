package com.moc.model;

import javax.persistence.*;


/*      id              INTEGER PRIMARY KEY,
        id_developer    INTEGER,
        CONSTRAINT fk_copm_developer FOREIGN KEY (id_developer)
        REFERENCES DEVELOPERS(id),
        id_tag         INTEGER,
        CONSTRAINT fk_copm_tags FOREIGN KEY (id_tag)
        REFERENCES TAGS(id),
        cost            DOUBLE,
        approved        BIT(1)*/
@Entity
public class Competency {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "id_developer")
    private Developer developer;

    @ManyToOne
    @JoinColumn(name = "id_tag")
    private Tag tag;

    @Column
    private Double cost;

    @Column
    private boolean approved;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Developer getDeveloper() {
        return developer;
    }

    public void setDeveloper(Developer developer) {
        this.developer = developer;
    }

    public Tag getTag() {
        return tag;
    }

    public void setTag(Tag tag) {
        this.tag = tag;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }
}
