package com.moc.model;

import java.io.Serializable;
import java.util.Objects;


public class ContractPK implements Serializable {
    private Long idTask;
    private Long idDeveloper;

    public ContractPK() {
    }

    public ContractPK(Long idTask, Long idDeveloper) {
        this.idTask = idTask;
        this.idDeveloper = idDeveloper;
    }

    public Long getIdDeveloper() {
        return idDeveloper;
    }

    public Long getIdTask(){
        return idTask;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ContractPK that = (ContractPK) o;
        return Objects.equals(idTask, that.idTask) &&
                Objects.equals(idDeveloper, that.idDeveloper);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idTask, idDeveloper);
    }
}
