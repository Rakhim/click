package com.moc.model;

import javax.persistence.*;
import java.util.Calendar;
import java.util.Set;

/*      id            INTEGER PRIMARY KEY,
        name          VARCHAR(40),
        id_customer   INTEGER,
        status        VARCHAR(40),
        description   VARCHAR(40),
        end_date      TIMESTAMP,
        last_update   TIMESTAMP*/
@Entity(name = "TASKS")
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String name;

    @ManyToOne
    @JoinColumn(name = "id_customer")
    private Customer customer;

    @Enumerated(EnumType.STRING)
    @Column
    private TaskStatus status;

    @Column
    private String description;

    @Column(name = "end_date")
    private Calendar endDate;

    @Column(name = "last_update")
    private Calendar lastUpdate;

    @ManyToOne
    @JoinColumn(name = "id_department")
    private Department department;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "TASK_TAGS", joinColumns = @JoinColumn(name = "id_task"),
            inverseJoinColumns = @JoinColumn(name = "id_tag"))
    private Set<Tag> tags;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public TaskStatus getStatus() {
        return status;
    }

    public void setStatus(TaskStatus status) {
        this.status = status;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Calendar getEndDate() {
        return endDate;
    }

    public void setEndDate(Calendar endDate) {
        this.endDate = endDate;
    }

    public Calendar getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Calendar lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void setTags(Set<Tag> tags) {
        this.tags = tags;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }
}
