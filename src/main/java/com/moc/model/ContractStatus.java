package com.moc.model;

public enum ContractStatus {
    Declined, Accepted, New, Done
}
