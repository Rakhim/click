package com.moc.model;

import javax.persistence.*;

/*
*  id        INTEGER PRIMARY KEY,
*  name      VARCHAR(40)
* */

@Entity(name = "DEPARTMENTS")
public class Department {
    @Id
    private Long id;

    @Column
    private String name;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
