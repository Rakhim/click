package com.moc.globals;

import com.moc.model.SystemUser;

public class GlobalContext {
    private static ThreadLocal<SystemUser> curUser = new ThreadLocal<>();

    public static SystemUser getCurUser() {
        return curUser.get();
    }

    public static void setCurUser(SystemUser curUser) {
        GlobalContext.curUser.set(curUser);
    }
}
