package com.moc.services;

import com.moc.model.Contract;
import com.moc.model.ContractPK;
import com.moc.repositories.ContractRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ContractService {

    @Resource
    private ContractRepository contractRepository;

    public void addNewContract(Contract contract){
        contractRepository.save(contract);
    }

    public Iterable<Contract> getAll()
    {
        return contractRepository.findAll();
    }

    public List<Contract> findDeveloperContractById(Long id)
    {
        return contractRepository.findDeveloperContractById(id);
    }

    public void sendFeedback(ContractPK contractId, Integer feedback){
        Contract contract = contractRepository.findById(contractId).get();
        contract.setFeedback(feedback);
        contractRepository.save(contract);
    }
}
