package com.moc.services;

import com.moc.model.Customer;
import com.moc.repositories.CustomerRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class CustomerService {

    @Resource
    CustomerRepository customerRepository;

    public void addNewCustomer(Customer customer) {
        customerRepository.save(customer);
    }

    public Iterable<Customer> getAll() {
        return customerRepository.findAll();
    }

    public Customer findById(Long id)
    {
        return customerRepository.findById(id).get();
    }
}
