package com.moc.services;

import com.moc.model.Department;
import com.moc.repositories.DepartmentRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class DepartmentService {

    @Resource
    private DepartmentRepository departmentRepository;

    public void addNewDepartment(){
        departmentRepository.save(new Department());
    }

    public Iterable<Department> getAll() {
        return departmentRepository.findAll();
    }

    public Department findByName(String department) {
        return departmentRepository.findByName(department);
    }
}
