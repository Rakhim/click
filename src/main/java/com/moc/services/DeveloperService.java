package com.moc.services;

import com.moc.model.Developer;
import com.moc.repositories.DeveloperRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class DeveloperService {

    @Resource
    private DeveloperRepository developerRepository;

    public Developer addNewDeveloper(Developer developer){
        return developerRepository.save(developer);
    }

    public Developer getDeveloperById(Long id){
        return developerRepository.findById(id).get();
    }
}
