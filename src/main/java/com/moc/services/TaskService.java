package com.moc.services;

import com.moc.model.Tag;
import com.moc.model.Task;
import com.moc.model.TaskStatus;
import com.moc.repositories.TaskRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class TaskService {

    @Resource
    private TaskRepository taskRepository;

    public Task saveTask(Task task){
        return taskRepository.save(task);
    }

    public Iterable<Task> getAll() {
        return taskRepository.findAll();
    }

    public Task findById(Long id) {
        final Optional<Task> taskOptional = taskRepository.findById(id);
        if (!taskOptional.isPresent()) {
            throw new IllegalArgumentException("");
        }
        return taskOptional.get();
    }

    public List<Task> findByCustomerId(Long id) {
        return taskRepository.findByCustomerId(id);
    }

    public void saveTagsForTask(Set<Tag> tagList, Long id){
        Task task = findById(id);
        task.setTags(tagList);
        saveTask(task);
    }

    public List<Task> findByStatusAndCustomerId(TaskStatus status, Long customerId){
        return taskRepository.findByStatus(status, customerId);
    }
}
