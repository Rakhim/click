package com.moc.services;

import com.moc.model.StopList;
import com.moc.repositories.StopListRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class StopListService {

    @Resource
    private StopListRepository stopListRepository;

    public void addStopList(){
        stopListRepository.save(new StopList());
    }
}
