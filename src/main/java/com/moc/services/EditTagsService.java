package com.moc.services;

import com.moc.services.stemming.StemmerPorterRU;
import com.moc.model.StopList;
import com.moc.model.Tag;
import com.moc.repositories.StopListRepository;
import com.moc.repositories.TagRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class EditTagsService {

    @Resource
    private TagRepository tagRepository;
    @Resource
    private StopListRepository stopListRepository;

    private static final int MIN_WORD_LENGTH = 2;
    private static final String WORDS_DELIMETER_REGEXP = "[\\s,!?.]";
    private Map<String, Tag> tags = new HashMap<>();
    private Map<String, Tag> basisTags = new HashMap<>();
    private final List<String> stopList = new ArrayList<>();
    private final StemmerPorterRU stemmerPorterRU = new StemmerPorterRU();

    public List<Tag> analyze(String text) {
        initialTagList(true);
        initialStopList();

        List<Tag> foundTags = new ArrayList<>();
        String[] words = text.split(WORDS_DELIMETER_REGEXP);
        for (String word : words)
        {
            word = word.toLowerCase();
            if (tags.keySet().contains(word)) {
                foundTags.add(tags.get(word));
                continue;
            }
            if (basisTags.keySet().contains(word))
            {
                foundTags.add(basisTags.get(word));
                continue;
            }

            //не анализируем короткие слова (У Ильи же длинный!) и слова из стоп-листа
            if (word.length() < MIN_WORD_LENGTH || stopList.contains(word)){
                continue;
            }

            String wordBasis = getWordBasis(word);
            if (tags.keySet().contains(wordBasis)) {
                foundTags.add(tags.get(wordBasis));
                continue;
            }

            if (basisTags.keySet().contains(wordBasis))
            {
                foundTags.add(basisTags.get(wordBasis));
                continue;
            }
        }

        return foundTags;
    }

    private String getWordBasis(String word) {
        return stemmerPorterRU.stem(word);
    }

    private void initialStopList() {
        for (StopList stopWord : stopListRepository.findAll())
        {
            stopList.add(stopWord.getWord().toLowerCase());
        }
    }

    private void initialTagList(boolean needBasicTags){
        for (Tag tag : tagRepository.findAll())
        {
            tags.put(tag.getName().toLowerCase(), tag);
            if (needBasicTags){
                basisTags.put(getWordBasis(tag.getName().toLowerCase()), tag);
            }
        }
    }

    public List<Tag> getTagList(){
        return (List<Tag>) tagRepository.findAll();
    }
}
