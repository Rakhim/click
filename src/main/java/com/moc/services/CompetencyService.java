package com.moc.services;

import com.moc.model.*;
import com.moc.repositories.CompetencyRepository;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

@Service
public class CompetencyService {

    @Resource
    private CompetencyRepository competencyRepository;

    public void addNewCompetency() {
        competencyRepository.save(new Competency());
    }

    public List<Developer> getCost(Task task) {
        Set<Tag> taskTags = task.getTags();
        List<Competency> competencyAllList = competencyRepository.findAll(taskTags);
        Map<Developer, List<Competency>> developerCompetencyMap = new HashMap<>();

        // Разбиение списка компетенций на разработчиков
        for (Competency competency : competencyAllList) {
            Developer developer = competency.getDeveloper();
            if (developer.getDepartment().equals(task.getDepartment())) {
                if (!developerCompetencyMap.containsKey(developer)) {
                    developerCompetencyMap.put(developer, new ArrayList<>());
                }
                developerCompetencyMap.get(developer).add(competency);
            }
        }

        List<Pair> developerCostList = new ArrayList<>();


        // Пробегаем по всем разработчикам, что бы у каждого вычислить значение его метрики относительно текущей задачи
        for (Developer developer : developerCompetencyMap.keySet()) {
            List<Competency> competencyList = developerCompetencyMap.get(developer);
            double costSum = 0;
            int competencyCount = 0;

            // Пробегаем по всем компетенциям разработчика, что бы вычислить сумму весов тэгов, необходимых для текущей задачи
            for (Competency competency : competencyList) {
                double cost = competency.getCost();
                // Если разработчик уже выполнял задачи, которое содержали такой тэг, то повышаем вес тэга
                if (competency.isApproved()) {
                    cost *= 1.1;
                }
                costSum += cost;
                competencyCount++;
            }

            int emptyCompetency = taskTags.size() - competencyCount;
            competencyCount += emptyCompetency;
            costSum /= competencyCount;

            // Если разработчик находится у заказчика в избранном, то тоже увеличиваем значение метрики
            if (task.getCustomer().getFavDevelopers().contains(competencyList.get(0).getDeveloper())) {
                costSum *= 1.1;
            }
            developerCostList.add(new Pair(developer, costSum));
        }

        Collections.sort(developerCostList, (x, y) -> {
            double value = x.second - y.second;
            return (int)(value * 1000);
        });

        List<Developer> resultList = new ArrayList<>();
        for (Pair pair : developerCostList){
            resultList.add(pair.getFirst());
        }

        return resultList;
    }

    private class Pair {
        private Developer first;
        private Double second;

        public Pair(Developer first, Double second) {
            this.first = first;
            this.second = second;
        }

        public Developer getFirst() {
            return first;
        }

        public Double getSecond() {
            return second;
        }
    }

    public void updateCostDeveloper(Developer developer, Task task, Integer feedback){
        List<Competency> competencies = competencyRepository.findAllByDeveloper(developer);
        Map<Tag, Competency> tagCompetencyMap = new HashMap<>();
        for (Competency competency : competencies){
            tagCompetencyMap.put(competency.getTag(), competency);
        }
        Set<Tag> tagSet = task.getTags();
        for (Tag tag : tagSet){
            if (tagCompetencyMap.containsKey(tag)){
                Competency competency = tagCompetencyMap.get(tag);
                competency.setCost((competency.getCost() + feedback) / 2);
            }
            else {
                Competency competency = new Competency();
                competency.setTag(tag);
                competency.setDeveloper(developer);
                competency.setApproved(true);
                competency.setCost((double) feedback);
                competencies.add(competency);
            }
        }
        competencyRepository.saveAll(competencies);
    }
}
