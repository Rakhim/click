package com.moc.services;

import com.moc.model.Tag;
import com.moc.repositories.TagRepository;

public class TagService {

    private TagRepository tagRepository;

    public TagService(TagRepository tagRepository){
        this.tagRepository = tagRepository;
    }

    public void addNewTag(){
        tagRepository.save(new Tag());
    }
}
