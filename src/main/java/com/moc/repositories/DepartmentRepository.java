package com.moc.repositories;

import com.moc.model.Department;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DepartmentRepository extends CrudRepository<Department, Long> {
    @Query("FROM DEPARTMENTS WHERE NAME IN ?1")
    Department findByName(String department);
}
