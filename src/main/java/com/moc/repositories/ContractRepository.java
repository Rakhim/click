package com.moc.repositories;

import com.moc.model.Contract;
import com.moc.model.ContractPK;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ContractRepository extends CrudRepository<Contract, ContractPK> {

    @Query("from CONTRACTS where id_developer = ?1")
    List<Contract> findDeveloperContractById(Long id);
}
