package com.moc.repositories;

import com.moc.model.Competency;
import com.moc.model.Developer;
import com.moc.model.Tag;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface CompetencyRepository extends CrudRepository<Competency, Long> {

    @Query("from Competency where tag in ?1")
    List<Competency> findAll(Set<Tag> tags);

    @Query("from Competency where developer=?1")
    public List<Competency> findAllByDeveloper(Developer developer);
}
