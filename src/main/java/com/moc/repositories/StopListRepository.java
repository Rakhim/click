package com.moc.repositories;

import com.moc.model.StopList;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StopListRepository extends CrudRepository<StopList, Long> {
}
