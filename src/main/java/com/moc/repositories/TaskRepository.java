package com.moc.repositories;

import com.moc.model.Task;
import com.moc.model.TaskStatus;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TaskRepository extends CrudRepository<Task, Long> {
    @Query("FROM TASKS WHERE id_customer IN ?1")
    List<Task> findByCustomerId(Long id);

    @Query("FROM TASKS WHERE status=?1 and customer.id=?2")
    List<Task> findByStatus(TaskStatus status, Long customerId);
}
