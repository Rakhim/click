package com.moc.controllers;

import com.google.gson.Gson;
import com.moc.dto.Status;
import com.moc.model.Developer;
import com.moc.services.DeveloperService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping
public class RegistrationController {

    @Autowired
    private DeveloperService developerService;

    /**
     * Регистрация разработчика
     * @param developerInfo -   инфа о разработчике
     * @return              -   Зарегистрированный разработчик
     */
    @GetMapping(value = "/registrationDeveloper", produces = "application/json; charset=UTF-8")
    public @ResponseBody
    String getDevelopers(@RequestParam(name = "developerInfo") String developerInfo) {
        Developer jsonDeveloper = new Gson().fromJson(developerInfo, Developer.class);
        Developer newDeveloper = developerService.addNewDeveloper(jsonDeveloper);

        return new Gson().toJson(newDeveloper);
    }
}