package com.moc.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.moc.dto.Status;
import com.moc.model.Tag;
import com.moc.model.Task;
import com.moc.model.TaskStatus;
import com.moc.services.CustomerService;
import com.moc.services.DepartmentService;
import com.moc.services.EditTagsService;
import com.moc.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;

/**
 * Управление задачами.
 */
@Controller
@RequestMapping
public class TaskController {

    @Autowired
    private TaskService service;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private EditTagsService editTagsService;

    private Set<Tag> getTags(String description) {
        Set<Tag> list = new HashSet<>();
        return list;
    }

    private Task getTask(String name, String description, String endDate, String department) {
        Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setEndDate(Calendar.getInstance());
        task.setStatus(TaskStatus.New);
        task.setTags(getTags(description));
        task.setCustomer(customerService.findById(1L));
        task.setDepartment(departmentService.findByName(department));
        return task;
    }

    /**
     * @param name
     * @param description
     * @param endDate
     * @param department
     * @return
     */
    @RequestMapping(value = "/task/add", produces = "application/json; charset=UTF-8")
    public @ResponseBody
    String add(
            @RequestParam(name = "name") String name,
            @RequestParam(name = "description") String description,
            @RequestParam(name = "endDate") String endDate,
            @RequestParam(name = "department") String department
    ) {
        Task newTask = service.saveTask(getTask(name, description, endDate, department));

        for (Task task : service.getAll()) {
            System.out.println(task.getDescription());
        }

        List<Tag> tagList = editTagsService.analyze(newTask.getDescription());
        return new GsonBuilder()
                .setPrettyPrinting()
                .create()
                .toJson(tagList
                        .stream()
                        .map(Tag::getName)
                        .toArray());
    }

    /**
     * Получение описания задачи
     * @param taskId    -   ид задачи
     * @return          -   описание задачи
     */
    @RequestMapping(value = "/task/info", produces = "application/json; charset=UTF-8")
    public @ResponseBody
    String info(@RequestParam(name = "taskId") Long taskId) {
        return new GsonBuilder().setPrettyPrinting().create().toJson(service.findById(taskId));
    }

    /**
     * Получение списока задач заказчика
     * @param сustomerId    -   ид заказчика
     * @return              -   список задач
     */
    @RequestMapping(value = "/task/list/customer", produces = "application/json; charset=UTF-8")
    public @ResponseBody
    String findByCustomerId(@RequestParam(name = "сustomerId") Long сustomerId) {
        return new GsonBuilder()
                .setPrettyPrinting()
                .create()
                .toJson(service.findByCustomerId(сustomerId));
    }

    /**
     * Получение списока завершенных задач
     * @param сustomerId сustomerId    -   ид заказчика
     * @return                         -   список завершенных задач
     */
    @RequestMapping(value = "/task/list/executed", produces = "application/json; charset=UTF-8")
    public @ResponseBody
    String findExecutedTasks(@RequestParam(name = "сustomerId") Long сustomerId){
        return new GsonBuilder()
                .setPrettyPrinting()
                .create()
                .toJson(service.findByStatusAndCustomerId(TaskStatus.Done, сustomerId));
    }

}
