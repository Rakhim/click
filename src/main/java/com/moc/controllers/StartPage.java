package com.moc.controllers;

import com.moc.globals.GlobalContext;
import com.moc.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Стартовая страница
 * todo нужен ли?
 */
@Controller
@RequestMapping
public class StartPage {

    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {

        //вошел заказчик
        //GlobalContext.setCurUser(customerService.findById(1L));

        return "jsp/tasks/index";
    }
}
