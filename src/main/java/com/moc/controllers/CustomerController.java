package com.moc.controllers;

import com.google.gson.Gson;
import com.moc.dto.Status;
import com.moc.model.Customer;
import com.moc.services.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Управление заказчиком.
 */
@Controller
@RequestMapping
public class CustomerController {

    @Autowired
    private CustomerService service;

    private Customer getCustomer(String taskInfo) {
        return new Gson().fromJson(taskInfo, Customer.class);
    }

    /**
     * Добавление заказчика
     * @param taskInfo  -   инофрмация о задаче (JSON)
     * @return          -   статус
     */
    @RequestMapping(value = "/customer/add", produces = "application/json; charset=UTF-8")
    public @ResponseBody String add(@RequestParam(name = "taskInfo") String taskInfo) {
        service.addNewCustomer(getCustomer(taskInfo));

        for (Customer customer : service.getAll()) {
            System.out.println(customer.getFirstName());
        }

        final Status status = new Status();
        status.setCode("0");
        return new Gson().toJson(status);
    }
}
