package com.moc.controllers;

import com.google.gson.GsonBuilder;
import com.moc.dto.Status;
import com.moc.model.Tag;
import com.moc.services.EditTagsService;
import com.moc.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Set;

/**
 * Контроллер для создания тэгов
 */
@Controller
@RequestMapping
public class TagController {

    @Autowired
    private EditTagsService editTagsService;

    @Autowired
    private TaskService taskService;

    /**
     * Получение тэгов для задачи посредством анализа текста задачи
     * @param textTask - текст задачи
     * @return jsp
     */
    @GetMapping(value = "/getTagsForTask", produces = "application/json; charset=UTF-8")
    public @ResponseBody
    String getTagsForTask(@RequestParam(name = "textTask") String textTask) {
        List<Tag> tagList = editTagsService.analyze(textTask);

        return new GsonBuilder().setPrettyPrinting().create().toJson(tagList);
    }

    /**
     * Сохранение тэгов для задачи
     * @param tags      - сэт тэгов
     * @param taskId    - id задачи
     * @return
     */
    @GetMapping(value = "/saveTagForTask", produces = "application/json; charset=UTF-8")
    public @ResponseBody
    String saveTagForTask(@RequestParam(name = "tags") Set<Tag> tags,
                          @RequestParam(name = "taskId") Long taskId) {
        taskService.saveTagsForTask(tags, taskId);

        Status status = new Status();
        status.setCode("0");

        return new GsonBuilder().setPrettyPrinting().create().toJson(status);
    }

    /**
     * Получить список всех тэгов
     * @return список тэгов
     */
    @GetMapping(value = "/getTagList", produces = "application/json; charset=UTF-8")
    public @ResponseBody
    String getTagList(){
        List<Tag> tagList = editTagsService.getTagList();

        return new GsonBuilder().setPrettyPrinting().create().toJson(tagList);
    }

}
