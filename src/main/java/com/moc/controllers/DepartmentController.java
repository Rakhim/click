package com.moc.controllers;

import com.google.gson.GsonBuilder;
import com.moc.model.Department;
import com.moc.services.DepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

/**
 * Управление подразделениями.
 */
@Controller
@RequestMapping
public class DepartmentController {

    @Autowired
    private DepartmentService departmentService;

    /**
     * Получение списка всех подразделдений.
     *
     * @return -   список имен подразделдений
     */
    @RequestMapping(value = "/department/list", produces = "application/json; charset=UTF-8")
    public @ResponseBody
    String list() {
        Iterable<Department> departments = departmentService.getAll();
        List<String> departmentNames = new ArrayList<>();
        for (Department department : departments) {
            departmentNames.add(department.getName());
        }

        return new GsonBuilder()
                .setPrettyPrinting()
                .create()
                .toJson(departmentNames);
    }
}