package com.moc.controllers;

import com.google.gson.GsonBuilder;
import com.moc.model.Developer;
import com.moc.services.CompetencyService;
import com.moc.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping
public class GetDevelopersController {

    @Autowired
    private TaskService taskService;

    @Autowired
    private CompetencyService competencyService;

    /**
     * Получение рекомендованных разработчиков на задачу
     * @param taskId    -   ид задачи
     * @return          -   список задач
     */
    @GetMapping(value = "/getDevelopers", produces = "application/json; charset=UTF-8")
    public @ResponseBody
    String getDevelopers(@RequestParam(name = "taskId") long taskId) {
        List<Developer> costTags = competencyService.getCost(taskService.findById(taskId));
        return new GsonBuilder()
                .setPrettyPrinting()
                .create()
                .toJson(costTags);
    }
}
