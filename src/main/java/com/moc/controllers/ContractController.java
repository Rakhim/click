package com.moc.controllers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.moc.dto.Status;
import com.moc.model.Contract;
import com.moc.model.ContractPK;
import com.moc.services.CompetencyService;
import com.moc.services.ContractService;
import com.moc.services.DeveloperService;
import com.moc.services.TaskService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping
public class ContractController {

    @Autowired
    private ContractService contractService;

    @Autowired
    private CompetencyService competencyService;

    @Autowired
    private DeveloperService developerService;

    @Autowired
    private TaskService taskService;

    @RequestMapping(value = "/contract/update", produces = "application/json; charset=UTF-8")
    public @ResponseBody
    String add(@RequestParam(name = "info") String contractInfo)
    {
        Contract jsonContract = new Gson().fromJson(contractInfo, Contract.class);
        contractService.addNewContract(jsonContract);

        for (Contract contract : contractService.getAll()) {
            System.out.println(contract.getIdTask() + " " + contract.getIdDeveloper() + " " + contract.getStatus());
        }

        final Status status = new Status();
        status.setCode("0");
        return new Gson().toJson(status);
    }

    @RequestMapping(value = "/contracts", produces = "application/json; charset=UTF-8")
    public @ResponseBody
    String getAllByDeveloperId(@RequestParam(name = "id") Long idDeveloper)
    {
        List<Contract> contracts = contractService.findDeveloperContractById(idDeveloper);

        return new GsonBuilder().setPrettyPrinting().create().toJson(contracts);
    }

    @RequestMapping(value = "/sendFeedback", produces = "application/json; charset=UTF-8")
    public @ResponseBody
    String sendFeedback(@RequestParam(name = "contractId") String contractId, @RequestParam(name = "feedback") Integer feedback){
        ContractPK contractPK = getContractId(contractId);
        contractService.sendFeedback(contractPK, feedback);
        competencyService.updateCostDeveloper(developerService.getDeveloperById(contractPK.getIdDeveloper()),
                taskService.findById(contractPK.getIdTask()), feedback);

        final Status status = new Status();
        status.setCode("0");
        return new Gson().toJson(status);
    }

    private ContractPK getContractId(String contractId){
        return new Gson().fromJson(contractId, ContractPK.class);
    }
}
